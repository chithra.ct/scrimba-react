import React from 'react'
function MyDetails() {
    return (
        <div>
            <p>Be happy for this moment</p>
            <ul>
                <li>Freedom</li>
                <li>Happiness</li>
                <li>Smile</li>
                <li>Hugs</li>
            </ul>
        </div>
    )
}
export default MyDetails