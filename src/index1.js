import React from 'react'
import ReactDom from 'react-dom'
import './style.css'
import MyDetails from './components/MyDetails'

function MyInfo() {
    return (
        <div>
            <h1>Chithra</h1>
            <p>An independent girl who loves her family and friends the most</p>
            <ul>
                <li>Humpi</li>
                <li>Manali</li>
                <li>Scotland</li>
            </ul>
        </div>
    )
}
ReactDom.render(< MyInfo />, document.getElementById("root"))
ReactDom.render(< MyDetails />, document.getElementById("root"))